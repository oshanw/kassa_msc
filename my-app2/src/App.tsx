import * as React from "react";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";

import { withStyles, WithStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import CircularProgress from "@material-ui/core/CircularProgress";

import "./App.css";
import PageTwo from "./components/PageTwo";

const styles = theme => ({
  root: {
    flexGrow: 1
  },

  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: 500
  },
  button: {
    margin: theme.spacing.unit,
    width: 100
  },
  progress: {
    margin: theme.spacing.unit * 20
  }
});

export interface Items {
  objectID?: string;
  title?: string;
  author?: string;
  num_comments?: number;
  created_at?: string;
}
export interface IPropsk extends WithStyles<typeof styles> {
  data?: Array<Items>;
  fetchData?(value: string): void;
}
export interface IState {
  isLoaded: boolean;
  hits: Array<Items>;
  value: string;
  error: any;
}

const API = "https://hn.algolia.com/api/v1/search?query=";

class App extends React.Component<IPropsk, IState> {
  constructor(props: IPropsk) {
    super(props);

    this.state = {
      isLoaded: true,
      error: null,
      hits: [],
      value: ""
    };

    this.handleChange = this.handleChange.bind(this);
  }

  fetchData = event => {
    this.setState({ isLoaded: false });

    fetch(API + event)
      .then(response => response.json())
      .then(data => this.setState({ hits: data.hits, isLoaded: true }));
  };

  handleChange(event) {
    this.setState({ value: event.target.value });
  }
  render() {
    return (
      <div>
        <div className={this.props.classes.root}>
          <AppBar position="static" color="default">
            <Toolbar>
              <Typography variant="title" color="textSecondary">
                Phase 1 Training 2018
              </Typography>
            </Toolbar>
          </AppBar>
        </div>

        <div>
          <TextField
            id="required"
            label="Type buzz word for find news"
            defaultValue="Hello World"
            className={this.props.classes.textField}
            autoComplete="off"
            margin="dense"
            value={this.state.value}
            onChange={this.handleChange}
          />

          <Button
            variant="contained"
            color="primary"
            onClick={() => this.fetchData(this.state.value)}
          >
            Search
          </Button>
        </div>
        <div>
          {!this.state.isLoaded && (
            <CircularProgress
              className={this.props.classes.progress}
              size={150}
            />
          )}
        </div>
        <div>{this.state.isLoaded && <PageTwo data={this.state.hits} />}</div>
      </div>
    );
  }
}

export default withStyles(styles)(App);

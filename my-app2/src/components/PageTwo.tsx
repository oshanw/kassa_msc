import * as React from "react";

import { withStyles, WithStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";

export interface Items {
  objectID?: string;
  title?: string;
  author?: string;
  num_comments?: number;
  created_at?: string;
}
export interface IProps extends WithStyles<typeof styles> {
  data: Array<Items>;
}
const styles = theme => ({
  root: {
    width: "80%",
    marginTop: theme.spacing.unit
  },
  table: {
    minWidth: 40
  }
});

class PageTwo extends React.Component<IProps> {
  constructor(props: IProps) {
    super(props);
  }

  render() {
    return (
      <Paper className={this.props.classes.root}>
        <Table className={this.props.classes.table}>
          <TableHead>
            <TableRow>
              <TableCell numeric>Title</TableCell>
              <TableCell numeric>Author </TableCell>
              <TableCell numeric>Created At </TableCell>
              <TableCell numeric>No of Comments</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {this.props.data.map(hit => {
              return (
                <TableRow key={hit.objectID}>
                  <TableCell component="th" scope="row">
                    {hit.title}
                  </TableCell>

                  <TableCell numeric>{hit.author}</TableCell>
                  <TableCell numeric>{hit.created_at}</TableCell>
                  <TableCell numeric>{hit.num_comments}</TableCell>
                </TableRow>
              );
            })}
          </TableBody>
        </Table>
      </Paper>
    );
  }
}

export default withStyles(styles)(PageTwo);
